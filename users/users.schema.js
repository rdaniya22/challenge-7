const registrationSchema = {
  email: {
    isEmail:true,
    errorMessage:"incorrect format email",
  },
  username: {
    isString:true,
    errorMessage:"username should be string",
  },
  password:{
    isLength:{options: { min:8}},
    errorMessage:"Password should be at least 8 characters",
  },
}

const loginSchema = {
 
  username: {
    isString:true,
    errorMessage:"username should be string",
  },
  password:{
    isLength:{options: { min:8}},
    errorMessage:"Password should be at least 8 characters",
  },
}

const updateBioSchema = {
 
  fullname: {
    isString:true,
    errorMessage:"full name should be string",
  },
  phonenumber: {
    isMobilePhone(),
    errorMessage:"incorrect format phone number",
  },
  
}
module.exports = {registrationSchema,loginSchema,updateBioSchema};