const express = require('express');
const userRouter = express.Router();
const userController = require('./controller');
const authMiddleware = require('../middleware/authMiddleware');
const { checkSchema } = require('express-validator');
const schemaValidation = require('../middleware/schemaValidation');
const { registrationSchema, loginSchema, updateBioSchema } = require('./users.schema');



//get all users
userRouter.get("/",authMiddleware, userController.getAllUsers);

//to register user
userRouter.post("/register",checkSchema(registrationSchema),schemaValidation, userController.registerUsers);

//to login user
userRouter.post("/login", checkSchema(loginSchema),schemaValidation,userController.login);

//to add game history
userRouter.post("/history", authMiddleware,userController.history);

//to add user bio
userRouter.post("/add-bio", authMiddleware,userController.addBio);

//get 1 bio
userRouter.get("/detail/:id",authMiddleware,userController.getSingleBio);

//update 1 bio
userRouter.put("/update/:id",checkSchema(updateBioSchema),schemaValidation,authMiddleware,userController.updateSingleBio);

//get all games from 1 id
userRouter.get("/game/:id",authMiddleware,userController.getAllGame);

module.exports = userRouter;