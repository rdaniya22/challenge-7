const $jq = jQuery.noConflict();

$jq(document).ready(function(){
  $jq('.slider').slick({
    arrows: true,
    dots:true,
    slidesToShow: 1,
    prevArrow: $jq('.prev'),
    nextArrow: $jq('.next'),

    responsive: [
      
      {
        breakpoint: 767,
        settings: {
          arrows: false
        },
      },
    ],

  });
});


function myFunction() {
  var x = document.getElementById("myLinks");
  if (x.style.display === "block") {
    x.style.display = "none";
  } else {
    x.style.display = "block";
  }
}