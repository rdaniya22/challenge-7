const jwtTool = require("jsonwebtoken");
const authMiddleware = async (req,res,next)=>{
  //check if there is authorization in header

  const {authorization} = req.headers;
  console.log(authorization);

  //if authorization is null/undefined
  if(authorization===undefined){
    return res.json({message: "you need login"})
  }
  try {
    const token = await jwtTool.verify(authorization,"shhh");
   
    next();
    
  } catch (error) {
    
   
    res.statusCode = 400;
    return res.json({message: "invalid token"});
    
  }
  //if there is valid authorization

};

module.exports = authMiddleware;